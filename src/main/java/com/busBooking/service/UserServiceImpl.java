package com.busBooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.busBooking.exceptions.UserNotFoundException;
import com.busBooking.model.User;
import com.busBooking.repository.UserRepo;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepo userRepository;

	public boolean finduserByEmail(String email) {

		User user = userRepository.findByEmail(email);
		if (user != null) {

			return true;

		} else {
			throw new UserNotFoundException("requested user is not there");

		}

	}

}
