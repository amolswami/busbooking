package com.busBooking.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.busBooking.model.Bus;
import com.busBookingDto.BusDto;
import com.busBookingDto.CreateBusDto;

@Service
public interface BusService {

	public Bus createBus(CreateBusDto createBusDto);

	public List<Bus> searchbus(BusDto busDto);

	public List<Bus> getBus();

}
