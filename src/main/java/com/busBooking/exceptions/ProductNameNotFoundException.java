package com.busBooking.exceptions;

public class ProductNameNotFoundException extends RuntimeException {

	public ProductNameNotFoundException(String msg) {
		super(msg);

	}

}
