package com.busBooking.serviceTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.busBooking.exceptions.BusNotFoundException;
import com.busBooking.model.Bus;
import com.busBooking.repository.BusRepo;
import com.busBooking.service.BusServiceImpl;
import com.busBookingDto.BusDto;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BusServiceTest {

	@InjectMocks
	BusServiceImpl busService;

	@Mock
	BusRepo busRepository;

	@Test(expected = BusNotFoundException.class)
	public void testsearchbusForPositive() {
		List<Bus> buses = new ArrayList();
		Bus bus = new Bus();
		BusDto busDto = new BusDto();
		bus.setBusid(1);
		// bus.setBusname("boopalbus");
		bus.setDestination("bangalore");
		bus.setSource("mumbai");
		buses.add(bus);
		Mockito.when(busRepository.findBusBySourceAndDestinationAndJourneydate("mumbai", "bangalore", null))
				.thenReturn(buses);
		List<Bus> buss = busService.searchbus(busDto);
		Assert.assertNotNull(buss);
		Assert.assertEquals(1, buss.size());

	}

	@Test
	public void testsearchbusForNagative() {
		List<Bus> buses = new ArrayList();
		Bus bus = new Bus();
		bus.setBusid(-1);
		BusDto busDto = new BusDto();
		// bus.setBusname("boopalbus");
		busDto.setDestination("bangalore");
		busDto.setSource("mumbai");
		busDto.setJourneyDate(null);
		buses.add(bus);
		Mockito.when(busRepository.findBusBySourceAndDestinationAndJourneydate("mumbai", "bangalore", null))
				.thenReturn(buses);
		List<Bus> buss = busService.searchbus(busDto);
		Assert.assertNotNull(buss);
		Assert.assertEquals(1, buss.size());

	}

}
