package com.busBooking.controllerTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.busBooking.controller.BookingController;
import com.busBooking.model.Booking;
import com.busBooking.service.BusBookingServiceImpl;
import com.busBookingDto.BookingDto;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingControllerTest {

	@InjectMocks
	BookingController bookingController;

	@Mock
	BusBookingServiceImpl busBookingService;

	@Test
	public void testBusbooking() {
		BookingDto bookingDto = new BookingDto();
		Booking booking = new Booking();
		bookingDto.setBusid(1);
		// bookingDto.setEmailId("sai@gmail.com");
		Mockito.when(busBookingService.makeBooking(bookingDto)).thenReturn(booking);
		Assert.assertEquals(booking, busBookingService.makeBooking(bookingDto));

	}

	@Test
	public void testBusbookingpos() {
		BookingDto bookingDto = new BookingDto();
		Booking booking = new Booking();
		bookingDto.setBusid(1);
		// bookingDto.setEmailId("sai@gmail.com");
		Mockito.when(busBookingService.makeBooking(bookingDto)).thenReturn(booking);
		Assert.assertNotNull(booking);
		ResponseEntity<Booking> b = bookingController.makeBooking(bookingDto);

		Assert.assertEquals(HttpStatus.OK, b.getStatusCode());

	}

}
