package com.busBookingDto;

import java.time.LocalDate;

public class BookingDto {

	private int busid;
	private int userid;

	private LocalDate bookingdate;
	private int bookseat;

	public int getBusid() {
		return busid;
	}

	public void setBusid(int busid) {
		this.busid = busid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public LocalDate getBookingdate() {
		return bookingdate;
	}

	public void setBookingdate(LocalDate bookingdate) {
		this.bookingdate = bookingdate;
	}

	public int getBookseat() {
		return bookseat;
	}

	public void setBookseat(int bookseat) {
		this.bookseat = bookseat;
	}

}
