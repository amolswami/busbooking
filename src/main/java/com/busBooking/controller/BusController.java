package com.busBooking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.busBooking.model.Bus;
import com.busBooking.service.BusService;
import com.busBooking.service.UserService;
import com.busBookingDto.BusDto;
import com.busBookingDto.ResponseMessageDto;

@RestController
public class BusController {

	@Autowired
	BusService busService;
	@Autowired
	UserService userService;

	@PostMapping(value = "/buses")
	public ResponseEntity<Object> searchbus(@RequestBody BusDto busDto, @RequestParam("email") String email) {
		ResponseMessageDto dto = new ResponseMessageDto();
		if (userService.finduserByEmail(email)) {
			List<Bus> buses = busService.searchbus(busDto);

			return new ResponseEntity<>(buses, HttpStatus.OK);
		} else {
			dto.setMessage("no access for you");
			return new ResponseEntity<>(dto, HttpStatus.BAD_REQUEST);

		}
	}
}