package com.busBooking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.busBooking.model.Booking;

@Repository
public interface BookingRepo extends JpaRepository<Booking, Integer>{

	@Query(value = "SELECT * FROM booking WHERE  userid=? " , nativeQuery = true)
	public List<Booking> findByUserid(int userid);
}
