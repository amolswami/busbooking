package com.busBookingDto;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.busBooking.model.Booking;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO {

	@JsonProperty(value = "userid")
	private Long UserId;
	@JsonProperty(value = "username")
	private String userName;
	@JsonProperty(value = "email")
	private String email;
	@JsonProperty(value = "phoneNo")
	private String phoneNo;
	@JsonProperty(value = "booking")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "userid")
	private Set<Booking> booking;

	public Long getUserId() {
		return UserId;
	}

	public void setUserId(Long userId) {
		UserId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Set<Booking> getBooking() {
		return booking;
	}

	public void setBooking(Set<Booking> booking) {
		this.booking = booking;
	}

}
