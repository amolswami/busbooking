package com.busBooking.exceptions;

public class DateNotFoundException extends RuntimeException {
	
	public DateNotFoundException(String msg) {
		super(msg);
		
	}

}
