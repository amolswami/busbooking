package com.busBooking.exceptions;

public class SeatsNotAvilableException extends RuntimeException {
	
	public SeatsNotAvilableException(String msg) {
		super(msg);
	}

}
