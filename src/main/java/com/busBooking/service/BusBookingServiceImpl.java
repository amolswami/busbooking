package com.busBooking.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.busBooking.exceptions.BookingNotFoundException;
import com.busBooking.exceptions.BusNotFoundException;
import com.busBooking.exceptions.DateNotFoundException;
import com.busBooking.exceptions.SeatsNotAvilableException;
import com.busBooking.exceptions.UserNotFoundException;
import com.busBooking.model.Booking;
import com.busBooking.model.Bus;
import com.busBooking.model.User;
import com.busBooking.repository.BookingRepo;
import com.busBooking.repository.BusRepo;
import com.busBooking.repository.UserRepo;
import com.busBookingDto.BookingDto;

@Service
public class BusBookingServiceImpl implements BusBookingService {

	@Autowired
	private BookingRepo busBookingRepo;
	@Autowired
	private BusRepo busRepo;
	@Autowired
	private UserRepo userRepo;

	@Override
	public Booking makeBooking(BookingDto bookingDto) {
		Booking booking = new Booking();
		BeanUtils.copyProperties(bookingDto, booking);

		User user = userRepo.findById(bookingDto.getUserid())
				.orElseThrow(() -> new UserNotFoundException("User is Not availble"));

		Bus bus = busRepo.findById(bookingDto.getBusid())
				.orElseThrow(() -> new BusNotFoundException("Bus is not available..."));

		if (bookingDto.getBookingdate().isBefore(LocalDate.now())) {
			throw new DateNotFoundException("previous date is not allowed..");
		}

		if (bus.getNoofseatsavailable() > bookingDto.getBookseat()) {

			bus.setNoofseatsavailable(bus.getNoofseatsavailable() - bookingDto.getBookseat());
			busRepo.save(bus);
			double fare = bus.getFare() * bookingDto.getBookseat();

			booking.setAmount(fare);
			booking.setUserid(user);
			booking.setBusid(bus);
			booking.setJourneydate(bookingDto.getBookingdate());
			booking.setBookseats(bookingDto.getBookseat());
			return busBookingRepo.save(booking);
		} else {
			throw new SeatsNotAvilableException("seats are not available");
		}

	}

	@Override
	public List<Booking> getBookingUserById(int userid) throws UserNotFoundException {

		User user = userRepo.findById(userid).orElseThrow(() -> new UserNotFoundException("User is Not availble"));

		List<Booking> option = busBookingRepo.findByUserid(userid);

		if (option.isEmpty()) {

			throw new BookingNotFoundException("bookings for particular date are not there");
		}

		return option;
	}

}
