package com.busBooking.exceptions;

import java.sql.Time;

public class BusNotFoundException extends RuntimeException{
	
	public BusNotFoundException(String source) {
		super(String.format(" %s",source));
	}

}