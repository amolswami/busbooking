package com.busBooking.service;

import java.util.List;

import com.busBooking.model.Booking;
import com.busBookingDto.BookingDto;

public interface BusBookingService {

	Booking makeBooking(BookingDto bookingDto);
	

	public List<Booking> getBookingUserById(int userid);

}
