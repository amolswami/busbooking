package com.busBooking.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bus")
public class Bus {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int busid;
	private String busname;
	private String source;
	private String destination;

	private LocalDate journeydate;
	private Double fare;
	private int totalnoofseats;
	private int noofseatsavailable;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "busid")
	private Set<Booking> booking;

	public int getBusid() {
		return busid;
	}

	public void setBusid(int busid) {
		this.busid = busid;
	}

	public String getBusname() {
		return busname;
	}

	public void setBusname(String busname) {
		this.busname = busname;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public LocalDate getJourneydate() {
		return journeydate;
	}

	public void setJourneydate(LocalDate journeydate) {
		this.journeydate = journeydate;
	}

	public Double getFare() {
		return fare;
	}

	public void setFare(Double fare) {
		this.fare = fare;
	}

	public int getTotalnoofseats() {
		return totalnoofseats;
	}

	public void setTotalnoofseats(int totalnoofseats) {
		this.totalnoofseats = totalnoofseats;
	}

	public int getNoofseatsavailable() {
		return noofseatsavailable;
	}

	public void setNoofseatsavailable(int noofseatsavailable) {
		this.noofseatsavailable = noofseatsavailable;
	}

	public Set<Booking> getBooking() {
		return booking;
	}

	public void setBooking(Set<Booking> booking) {
		this.booking = booking;
	}

}
