package com.busBooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.busBooking.exceptions.UserNotFoundException;
import com.busBooking.model.User;
import com.busBooking.repository.UserRepo;
import com.busBookingDto.UserLoginDto;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	UserRepo loginRepository;

	@Override
	public User login(UserLoginDto userLoginDto) throws UserNotFoundException {

		User login = loginRepository.findByEmailAndPassword(userLoginDto.getEmail(), userLoginDto.getPassword());
		if (login == null) {
			throw new UserNotFoundException("User doesnot exists");
		}

		return login;

	}

}
