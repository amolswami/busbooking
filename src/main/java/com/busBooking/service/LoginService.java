package com.busBooking.service;

import org.springframework.stereotype.Service;

import com.busBooking.exceptions.UserNotFoundException;
import com.busBooking.model.User;
import com.busBookingDto.UserLoginDto;

@Service
public interface LoginService {

	User login(UserLoginDto userLoginDto) throws UserNotFoundException;

}
