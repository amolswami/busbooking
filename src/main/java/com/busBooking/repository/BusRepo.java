package com.busBooking.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.busBooking.model.Bus;

@Repository
public interface BusRepo extends JpaRepository<Bus, Integer>{
	
	List<Bus> findBusBySourceAndDestinationAndJourneydate(String source, String destination, LocalDate date);

}
