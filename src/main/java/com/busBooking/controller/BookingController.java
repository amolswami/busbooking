package com.busBooking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.busBooking.exceptions.UserNotFoundException;
import com.busBooking.model.Booking;
import com.busBooking.service.BusBookingService;
import com.busBooking.service.UserService;
import com.busBookingDto.BookingDto;

@RestController
public class BookingController {

	@Autowired
	BusBookingService busBookingService;

	@Autowired
	UserService userService;

	@PostMapping("/booking")
	public ResponseEntity<Booking> makeBooking(@RequestBody BookingDto bookingDto) {
		Booking booking = busBookingService.makeBooking(bookingDto);

		return new ResponseEntity<>(booking, HttpStatus.OK);

	}

	@GetMapping(value = "/bookingUser/{userid}")
	public ResponseEntity<List<Booking>> getUserById(@PathVariable("userid") int userid) throws UserNotFoundException {
		List<Booking> b = busBookingService.getBookingUserById(userid);
		return new ResponseEntity<>(b, HttpStatus.OK);
	}

}
