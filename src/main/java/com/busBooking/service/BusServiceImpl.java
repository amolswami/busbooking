package com.busBooking.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.busBooking.exceptions.BusNotFoundException;
import com.busBooking.exceptions.DateNotFoundException;
import com.busBooking.model.Bus;
import com.busBooking.repository.BusRepo;
import com.busBookingDto.BusDto;
import com.busBookingDto.CreateBusDto;

@Service
public class BusServiceImpl implements BusService {

	@Autowired
	BusRepo busRepository;

	@Override
	public Bus createBus(CreateBusDto createBusDto) {
		Bus bus = new Bus();
		BeanUtils.copyProperties(createBusDto, bus);

		return busRepository.save(bus);
	}

	@Override
	public List<Bus> searchbus(BusDto busDto) throws com.busBooking.exceptions.BusNotFoundException {

		LocalDate currentdate = busDto.getJourneyDate();
		List<Bus> buses = busRepository.findBusBySourceAndDestinationAndJourneydate(busDto.getSource(),
				busDto.getDestination(), currentdate);

		if (currentdate.isBefore(LocalDate.now())) {
			throw new DateNotFoundException("previous date is not allowed..");

		}

		if (buses.isEmpty()) {
			throw new BusNotFoundException("buses are not available");
		}
		return buses;
	}

	@Override
	public List<Bus> getBus() {
		List<Bus> buses = busRepository.findAll();

		if (buses.isEmpty()) {
			throw new BusNotFoundException("buses are not available");
		}

		return buses;
	}

}
