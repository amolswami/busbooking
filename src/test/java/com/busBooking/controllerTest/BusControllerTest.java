package com.busBooking.controllerTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.busBooking.controller.BusController;
import com.busBooking.model.Bus;
import com.busBooking.service.BusService;
import com.busBooking.service.UserService;
import com.busBookingDto.BusDto;
import com.busBookingDto.ResponseMessageDto;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BusControllerTest {

	@InjectMocks
	BusController busController;
	@Mock
	BusService busService;
	@Mock
	UserService userService;

	@Test
	public void testlistbysourseps() {
		List<Bus> busses = new ArrayList<Bus>();
		Bus bus = new Bus();
		bus.setBusid(1);
		bus.setBusname("pk");
		busses = new ArrayList<Bus>();
		busses.add(bus);
		ResponseMessageDto dto = new ResponseMessageDto();
		BusDto busDto = new BusDto();

		Mockito.when(busService.searchbus(busDto)).thenReturn(busses);

		ResponseEntity<Object> p1 = busController.searchbus(busDto, "sk");
		Assert.assertNotNull(p1);
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		// Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());

	}

	@Test
	public void testbuslistbysrcneg() {
		List<Bus> busses = new ArrayList<Bus>();
		Bus bus = new Bus();
		bus.setBusid(1);
		bus.setBusname("pk");
		busses = new ArrayList<Bus>();
		busses.add(bus);
		ResponseMessageDto dto = new ResponseMessageDto();
		BusDto busDto = new BusDto();
		Mockito.when(busService.searchbus(busDto)).thenReturn(busses);

		ResponseEntity<Object> p1 = busController.searchbus(busDto, "sk");
		dto.setMessage("hi");
		Assert.assertNotNull(dto);
		Assert.assertNotNull(p1);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, p1.getStatusCode());

	}

}
