package com.busBooking.serviceTest;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;

import com.busBooking.model.Booking;
import com.busBooking.model.Bus;
import com.busBooking.model.User;
import com.busBooking.repository.BookingRepo;
import com.busBooking.repository.BusRepo;
import com.busBooking.repository.UserRepo;
import com.busBooking.service.BusBookingServiceImpl;
import com.busBooking.service.UserServiceImpl;
import com.busBookingDto.BookingDto;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingServiceTest {

	@InjectMocks
	BusBookingServiceImpl busService;

	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	BookingRepo bookRepository;

	@Mock
	BusRepo busRepo;

	@Mock
	UserRepo userRepository;

	@Test
	public void bookBusServicef() {
		BookingDto bookingDto = new BookingDto();

		bookingDto.setBookseat(2);
		;
		bookingDto.setBusid(1);
		Booking booking = new Booking();
		User usr = new User();
		BeanUtils.copyProperties(bookingDto, booking);
		Bus b = new Bus();
		b.setFare(100.0);

		Bus bus = new Bus();
		bus.setBusid(1);

		Mockito.when(busRepo.findById(1)).thenReturn(Optional.of(bus));
		busRepo.save(b);
		booking.setAmount(b.getFare() * bookingDto.getBookseat());
		booking.setUserid(usr);
		booking.setBusid(b);

		Assert.assertNotNull(usr);
		// Assert.assertEquals(booking, busService.makeBooking(bookingDto));

	}

}